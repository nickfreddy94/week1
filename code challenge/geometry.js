var readline = require("readline");

var rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

console.log("Geometry");
console.log("============");
console.log("1. Beam");
console.log("2. Cube");
console.log("3. tube");
console.log("4. Hemisphere");
const pi = 3.141592653589793;
rl.question("Pilihan anda : ", function (pilihan) {
  console.log("Anda Memilih Menu  " + pilihan);
  rl.question("Masukan nilai panjang/jari-jari : ", function (P) {
    rl.question("Masukan nilai lebar/jari-jari : ", function (L) {
      rl.question("Masukan nilai tnggi : ", function (T) {
        console.log("-----------------------------------------");
        console.log("Nilai panjang/ jari- jari  " + P);
        console.log("Nilai lebar/ jari - jari  " + L);
        console.log("Nilai Tinggi  " + T);
        console.log("-----------------------------------------");
        if (pilihan == "1") {
          var v1 = parseInt(P) * parseInt(L) * parseInt(T);
          console.log("Hasil Volume Beam :" + v1);
        } else if (pilihan == "2") {
          var v2 = parseInt(P) * parseInt(L) * parseInt(T);
          console.log("Hasil Volume Cube :" + v2);
        } else if (pilihan == "3") {
          var v3 = pi * parseInt(P) * parseInt(L) * parseInt(T);
          console.log("Hasil Volume tube :" + v3);
        } else if (pilihan == "4") {
          var v4 = (2 * pi * Math.pow(parseInt(P), 3)) / 3;
          console.log("Hasil Volume Hemisphere :" + v4);
        } else {
          console.log("Pilihan anda tidak tersedia");
        }

        rl.close();
      });
    });
  });
});
